%{?mingw_package_header}

#%%global snapshot_date 20160204
#%%global snapshot_rev 38410ad06264949efcb331f7a63575c6be31c5e4
#%%global snapshot_rev_short %(echo %snapshot_rev | cut -c1-6)
#%%global branch trunk

#%%global pre rc2

# Run the testsuite
%global enable_tests 0

%global enable_pthread_shared 1

Name:           mingw-winpthreads
Version:        11.0.0
Release:        3
Summary:        MinGW pthread library

# The main license of winpthreads is MIT, but parts of this library
# are derived from the "Posix Threads library for Microsoft Windows"
# http://locklessinc.com/articles/pthreads_on_windows/
License:        MIT and BSD
URL:            http://mingw-w64.sourceforge.net/
%if 0%{?snapshot_date}
# To regenerate a snapshot:
# Use your regular webbrowser to open https://sourceforge.net/p/mingw-w64/mingw-w64/ci/%{snapshot_rev}/tarball
# This triggers the SourceForge instructure to generate a snapshot
# After that you can pull in the archive with:
# spectool -g mingw-headers.spec
Source0:        http://sourceforge.net/code-snapshots/git/m/mi/mingw-w64/mingw-w64.git/mingw-w64-mingw-w64-%{snapshot_rev}.zip
%else
Source0:        http://downloads.sourceforge.net/mingw-w64/mingw-w64-v%{version}%{?pre:-%{pre}}.tar.bz2
%endif

BuildArch:      noarch

BuildRequires: make
BuildRequires:  mingw32-filesystem >= 95
BuildRequires:  mingw64-filesystem >= 95
BuildRequires:  mingw32-gcc-c++
BuildRequires:  mingw64-gcc-c++

%if 0%{?enable_tests}
BuildRequires:  wine-wow
%endif


%description
The POSIX 1003.1-2001 standard defines an application programming
interface (API) for writing multithreaded applications. This interface
is known more commonly as pthreads. A good number of modern operating
systems include a threading library of some kind: Solaris (UI)
threads, Win32 threads, DCE threads, DECthreads, or any of the draft
revisions of the pthreads standard. The trend is that most of these
systems are slowly adopting the pthreads standard API, with
application developers following suit to reduce porting woes.

Win32 does not, and is unlikely to ever, support pthreads
natively. This project seeks to provide a freely available and
high-quality solution to this problem.

# Win32
%package -n mingw32-winpthreads
Summary:        MinGW pthread library for the win32 target
Obsoletes:      mingw32-pthreads < 2.8.0-25.20110511cvs
Provides:       mingw32-pthreads = 2.8.0-25.20110511cvs
Conflicts:      mingw32-headers < 2.0.999-0.22.trunk.20130428

%description -n mingw32-winpthreads
The POSIX 1003.1-2001 standard defines an application programming
interface (API) for writing multithreaded applications. This interface
is known more commonly as pthreads. A good number of modern operating
systems include a threading library of some kind: Solaris (UI)
threads, Win32 threads, DCE threads, DECthreads, or any of the draft
revisions of the pthreads standard. The trend is that most of these
systems are slowly adopting the pthreads standard API, with
application developers following suit to reduce porting woes.

Win32 does not, and is unlikely to ever, support pthreads
natively. This project seeks to provide a freely available and
high-quality solution to this problem.

%package -n mingw32-winpthreads-static
Summary:        Static version of the MinGW Windows pthreads library
Requires:       mingw32-winpthreads = %{version}-%{release}
Obsoletes:      mingw32-pthreads-static < 2.8.0-25.20110511cvs
Provides:       mingw32-pthreads-static = 2.8.0-25.20110511cvs

%description -n mingw32-winpthreads-static
Static version of the MinGW Windows pthreads library.

# Win64
%package -n mingw64-winpthreads
Summary:        MinGW pthread library for the win64 target
Obsoletes:      mingw64-pthreads < 2.8.0-25.20110511cvs
Provides:       mingw64-pthreads = 2.8.0-25.20110511cvs
Conflicts:      mingw64-headers < 2.0.999-0.22.trunk.20130428

%description -n mingw64-winpthreads
The POSIX 1003.1-2001 standard defines an application programming
interface (API) for writing multithreaded applications. This interface
is known more commonly as pthreads. A good number of modern operating
systems include a threading library of some kind: Solaris (UI)
threads, Win32 threads, DCE threads, DECthreads, or any of the draft
revisions of the pthreads standard. The trend is that most of these
systems are slowly adopting the pthreads standard API, with
application developers following suit to reduce porting woes.

Win32 does not, and is unlikely to ever, support pthreads
natively. This project seeks to provide a freely available and
high-quality solution to this problem.

%package -n mingw64-winpthreads-static
Summary:        Static version of the MinGW Windows pthreads library
Requires:       mingw64-winpthreads = %{version}-%{release}
Obsoletes:      mingw64-pthreads-static < 2.8.0-25.20110511cvs
Provides:       mingw64-pthreads-static = 2.8.0-25.20110511cvs

%description -n mingw64-winpthreads-static
Static version of the MinGW Windows pthreads library.

%if 0%{enable_pthread_shared} == 1
%?mingw_debug_package
%endif

%prep
%if 0%{?snapshot_date}
rm -rf mingw-w64-v%{version}
mkdir mingw-w64-v%{version}
cd mingw-w64-v%{version}
unzip %{S:0}
%autosetup -p1 -D -T -n mingw-w64-v%{version}/mingw-w64-mingw-w64-%{snapshot_rev}
%else
%autosetup -p1 -n mingw-w64-v%{version}%{?pre:-%{pre}}
%endif


%build
pushd mingw-w64-libraries/winpthreads
    %mingw_configure \
%if 0%{enable_pthread_shared} == 0
    --enable-shared=no
%endif
    %mingw_make_build
popd


%if 0%{?enable_tests}

%check
# Prepare a wine prefix
export WINEPREFIX=/tmp/wine-winpthreads
mkdir $WINEPREFIX
winecfg || :

# Run the tests
pushd mingw-w64-libraries/winpthreads
    %mingw_make check -k || :
popd

# Clean up the wine prefix
wineserver --kill || :
rm -rf /tmp/wine-winpthreads

%endif


%install
pushd mingw-w64-libraries/winpthreads
    %mingw_make_install DESTDIR=%{buildroot}
popd

# Drop all .la files
find $RPM_BUILD_ROOT -name "*.la" -delete


# Win32
%files -n mingw32-winpthreads
%license COPYING
%if 0%{enable_pthread_shared} == 1
%{mingw32_bindir}/libwinpthread-1.dll
%{mingw32_libdir}/libwinpthread.dll.a
%{mingw32_libdir}/libpthread.dll.a
%endif
%{mingw32_includedir}/pthread.h
%{mingw32_includedir}/pthread_compat.h
%{mingw32_includedir}/pthread_signal.h
%{mingw32_includedir}/pthread_time.h
%{mingw32_includedir}/pthread_unistd.h
%{mingw32_includedir}/sched.h
%{mingw32_includedir}/semaphore.h

%files -n mingw32-winpthreads-static
%{mingw32_libdir}/libwinpthread.a
%{mingw32_libdir}/libpthread.a

# Win64
%files -n mingw64-winpthreads
%license COPYING
%if 0%{enable_pthread_shared} == 1
%{mingw64_bindir}/libwinpthread-1.dll
%{mingw64_libdir}/libwinpthread.dll.a
%{mingw64_libdir}/libpthread.dll.a
%endif
%{mingw64_includedir}/pthread.h
%{mingw64_includedir}/pthread_compat.h
%{mingw64_includedir}/pthread_signal.h
%{mingw64_includedir}/pthread_time.h
%{mingw64_includedir}/pthread_unistd.h
%{mingw64_includedir}/sched.h
%{mingw64_includedir}/semaphore.h

%files -n mingw64-winpthreads-static
%{mingw64_libdir}/libwinpthread.a
%{mingw64_libdir}/libpthread.a


%changelog
* Fri Mar 07 2025 yangfeng <yangfeng@kylinsec.com.cn> - 11.0.0-3
- set enable_pthread_shared to 1 for compile dynamic library

* Fri Mar 07 2025 yangfeng <yangfeng@kylinsec.com.cn> - 11.0.0-2
- set enable_pthread_shared to 0 for compile static library

* Thu Sep 19 2024 kkz <zhaoshuang@uniontech.com> - 11.0.0-1
- update version to 11.0.0
  update according to upstream

* Tue Feb 27 2024 yinhongchang <yinhongchang@kylinsec.com.cn> - 9.0.0-4
- set enable_pthread_shared to 1 for compile dynamic library

* Wed Feb 21 2024 yinhongchang <yinhongchang@kylinsec.com.cn> - 9.0.0-3
- update version to 9.0.0

* Wed Nov 04 2020 Zhiyi Weng <zhiyi@iscas.ac.cn> - 6.0.0-4
- Add build require mingw-crt for bootstrap.

* Fri Oct 09 2020 Zhiyi Weng <zhiyi@iscas.ac.cn> - 6.0.0-3
- Initial version.
